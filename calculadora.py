"Calculadora ejemplo para trabajar git"

import sys

class Calculadora(object):

    def __init__(self):
        pass

    def suma(self,a,b):
        c=a+b
        return c

    def resta(self, a, b):
        result = float(a) - float(b)
        return round(result,1)

    def division(self,a,b):
        result = None
        if (type(a) is int) and (type(b) is int):
            if b != 0:
                result = a/b
        return result                    
        
    def mult(self,a,b): #multiplicacion
        mul = float(a)*float(b)
        return mul

if __name__ == '__main__':

    a = sys.argv[1]
    b = sys.argv[2]
    operation = sys.argv[3]
    print(a, b, operation)

    calc = Calculadora()

    result = calc.__getattribute__(operation)(a,b)
    print(result)

