import unittest
from calculadora import Calculadora
import sys 

class TestCalculadora(unittest.TestCase):
    
    def test_resta(self):
        calc = Calculadora()
        self.assertEqual(calc.resta(3,4), -1.0)
        self.assertEqual(calc.resta("4","4"), 0.0)
        self.assertEqual(calc.resta("4.3","4"), 0.3)

    def test_mult(self):
        #test case

        a = 2
        b = 3
        c = 6    
        #exec
        calc = Calculadora()
        d = calc.mult(a,b)
        mensaje = "el resultado es el mismo"
        self.assertEqual(c,d,mensaje)

    def test_valid_division(self):
        calculadora = Calculadora()        
        self.assertAlmostEqual(calculadora.division(4,2),2)
        
    def test_zero_division(self):
        calculadora = Calculadora()        
        self.assertAlmostEqual(calculadora.division(4,0),None) 
    
    def test_char_division_a(self):
        calculadora = Calculadora()        
        self.assertAlmostEqual(calculadora.division(4,''),None)        
        
    def test_char_division_b(self):
        calculadora = Calculadora()        
        self.assertAlmostEqual(calculadora.division('',4),None)                
    
    def test_char_division_a_and_b(self):
        calculadora = Calculadora()        
        self.assertAlmostEqual(calculadora.division('',4),None)

    def test_sum(self):
        # Test Case
        a = 2 
        b = 3
        c= 5
        # Execute function
        calc = Calculadora()
        d = calc.suma(a,b)   
        self.assertEqual(c,d)
